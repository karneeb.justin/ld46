extends TileMap

var _offset = Vector2(G.HGridSize, G.HGridSize)
var air_tiles = [-1]
var collider_tiles = []

func _ready():
    var tileset: TileSet = self.tile_set
    for i in tileset.get_tiles_ids():
        var shapes = tileset.tile_get_shape_count(i)
        if shapes > 0:
            collider_tiles.append(i)
        else:
            air_tiles.append(i)

func find_nearest_valid(world_pos: Vector2, is_flying: bool):
    var tile_pos = self.world_to_map(world_pos)
    
    if is_valid(tile_pos, is_flying):
        return self.map_to_world(tile_pos)
        
    for i in range(1, 10):
        var check_pos = Vector2(tile_pos.x, tile_pos.y - i)
        if is_valid(check_pos, is_flying):
            return self.map_to_world(check_pos)
        check_pos.y += 2*i
        if is_valid(check_pos, is_flying):
            return self.map_to_world(check_pos)
            
    return null

func map_to_world(cell: Vector2, ignore_half_ofs: bool=false) -> Vector2:
    return .map_to_world(cell, ignore_half_ofs) + _offset

func is_valid(tile_pos: Vector2, is_flying: bool) -> bool:
    var tile = self.get_cellv(tile_pos)
    if is_flying:
        if air_tiles.has(tile):
            return true
    elif air_tiles.has(tile):
        var under_tile = self.get_cell(int(tile_pos.x), int(tile_pos.y+1))
        return collider_tiles.has(under_tile)
    
    return false
