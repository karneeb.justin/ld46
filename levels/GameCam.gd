extends Camera2D

export(float) var scroll_speed: float = 64*20
export(bool) var edge_scroll: bool = false

signal showcase_finished

var grabbed: bool = false
var grabbed_mpos: Vector2
var grabbed_cpos: Vector2

var _min_pos: Vector2 = Vector2(0, 0)
var _max_pos: Vector2 = Vector2(0, 0)

var _showcase_path: Array = []
var _showcase_times: Array = []
var _showcase_remaining: float = 0
var _showcase_time: float = 0
var _showcase_delay: float = 0

var _interp_pre: Vector2 = Vector2()
var _interp_pos: Vector2 = Vector2()
var _interp_dur: float = 0
var _interp_cur: float = 0

func _process(delta):
    if _showcase_path.size() > 1:
        if _showcase_delay <= 0:
            self.set_pos(_showcase_path[0].linear_interpolate(_showcase_path[1], 1-_showcase_remaining/_showcase_times[0]))
            _showcase_remaining -= delta
            
            while _showcase_remaining <= 0:
                _showcase_path.pop_front()
                _showcase_times.pop_front()
                if not _showcase_times.empty():
                    _showcase_remaining = _showcase_times[0]
                else:
                    emit_signal("showcase_finished")
                    break
        _showcase_delay -= delta
        
    if _interp_cur > 0:
        set_pos(_interp_pre.linear_interpolate(_interp_pos, 1-_interp_cur/_interp_dur))
        _interp_cur -= delta

func interp_to(pos: Vector2, duration: float):
    _interp_pos = clamp_to_extent(pos)
    _interp_pre = position
    _interp_dur = duration
    _interp_cur = _interp_dur
    
func interping() -> bool:
    return _interp_cur > 0
 
func showcase(path: Array, showcase_time: float, showcase_delay: float) -> void:
    _showcase_path = path
    _showcase_time = showcase_time
    _showcase_delay = showcase_delay
    
    for i in range(path.size()):
        path[i] = clamp_to_extent(path[i].position - G.HScreen)
        
    var dists = []
    var dist_total = 0
    for i in range(path.size()-1):
        var dist = path[i].distance_to(path[i+1])
        dists.append(dist)
        dist_total += dist
        
    var to_remove = []
    for i in range(path.size()-1):
        if dists[i] <= 0:
            to_remove.append(i)
        else:
            _showcase_times.append(dists[i]/dist_total * _showcase_time)
    for i in to_remove:
        dists.remove(i)
        path.remove(i)
    
    if not _showcase_times.empty():
        _showcase_remaining = _showcase_times[0]
        self.set_pos(path[0])
    else:
        emit_signal("showcase_finished")
        
func stop_showcase():
    _showcase_remaining = -1
    _showcase_times = []
    set_pos(_showcase_path.pop_back())
    _showcase_path = []

func set_extent(extent: Rect2):
    self.limit_left = int(extent.position.x)
    self.limit_right = int(extent.end.x)
    self.limit_top = int(extent.position.y)
    self.limit_bottom = int(extent.end.y)
    
    _min_pos.x = extent.position.x
    _min_pos.y = extent.position.y
    _max_pos.x = extent.end.x - 1280
    _max_pos.y = extent.end.y - 720

func clamp_to_extent(pos: Vector2):
    pos.x = clamp(floor(pos.x), _min_pos.x, _max_pos.x)
    pos.y = clamp(floor(pos.y), _min_pos.y, _max_pos.y)
    return pos
    
func set_pos(pos: Vector2):
    self.position.x = clamp(floor(pos.x), _min_pos.x, _max_pos.x)
    self.position.y = clamp(floor(pos.y), _min_pos.y, _max_pos.y)
    G.CamPos = self.position

func is_showcasing() -> bool:
    return self._showcase_path.size() > 1
    
func _physics_process(delta):
    _handle_key_movement(delta)
    if grabbed:
        var mpos = get_viewport().get_mouse_position()
        var mdist = mpos - grabbed_mpos
        self.set_pos(grabbed_cpos - mdist)
    elif edge_scroll:
        var mpos = get_viewport().get_mouse_position()
        if mpos.x > G.Screen.x*0.95:
            position += Vector2(self.scroll_speed*delta, 0)
            self.set_pos(position)
        elif mpos.x < G.Screen.x*0.05:
            position += Vector2(-self.scroll_speed*delta, 0)
            self.set_pos(position)
        if mpos.y > G.Screen.y*0.95:
            position += Vector2(0, self.scroll_speed*delta)
            self.set_pos(position)
        elif mpos.y < G.Screen.y*0.05:
            position += Vector2(0, -self.scroll_speed*delta)
            self.set_pos(position)
            
    
func _unhandled_input(event):
    if event.is_action_pressed("grab"):
        grabbed = true
        grabbed_mpos = get_viewport().get_mouse_position()
        grabbed_cpos = self.position
    elif event.is_action_released("grab"):
        grabbed = false

var _key_movement := Vector2(0, 0)
func _handle_key_movement(delta: float):
    var moved := false
    if Input.is_action_pressed("up"):
        _key_movement.y = -1
        moved = true
    elif Input.is_action_pressed("down"):
        _key_movement.y = 1
        moved = true
    if Input.is_action_pressed("right"):
        _key_movement.x = 1
        moved = true
    elif Input.is_action_pressed("left"):
        _key_movement.x = -1
        moved = true
    
    if moved:
        _key_movement = _key_movement.normalized()
        self.position += _key_movement * self.scroll_speed * delta
        self.set_pos(self.position)
        _key_movement.x = 0
        _key_movement.y = 0
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
