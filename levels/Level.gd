extends Node2D

var PlayerScene: PackedScene = preload("res://player/Player.tscn")
export(bool) var showcase_map: bool = false
export(float) var showcase_time: float = 3
export(float) var showcase_delay: float = 0.5
export(bool) var follow_ducky: bool = false
export(bool) var demo: bool = false
export(float) var skip_timer: float = 1

onready var Cam = $GameCam
onready var Map = $TileMap
onready var BGMap = $BGTileMap
onready var UI = $CanvasLayer/UI
onready var Hotbar = $CanvasLayer/UI/Hotbar
onready var Instructions = $CanvasLayer/UI/Instructions
onready var LevelComplete = $CanvasLayer/UI/LevelComplete
onready var RetryBtn = $CanvasLayer/UI/LevelComplete/VBox/Center/HBox/Retry
onready var NextLvlBtn = $CanvasLayer/UI/LevelComplete/VBox/Center/HBox/Next
onready var Start = $TileMap/Start
onready var Goal = $TileMap/Goal
onready var PlayerContainer = $TileMap/PlayerContainer
onready var Waypoints = $TileMap/Waypoints
onready var Player = PlayerScene.instance()

var level_complete: bool = false
var _tool: Node
var _click_timer = INF

func _ready():
    randomize()
    var _err = Hotbar.connect("tool_clicked", self, "tool_clicked")    
    var tilemap_info = _calc_max_tilemap_size()
    var stage_size = tilemap_info[0]
    var stage_pos = tilemap_info[1]
    
    Cam.set_extent(Rect2(stage_pos, stage_size))    
    G.SpawnPos = Start.position
    Player.hide()
    PlayerContainer.add_child(Player)
    
    if showcase_map:
        var showcase_path = [Goal]
        for pos in Waypoints.get_children():
            showcase_path.append(pos)
        showcase_path.append(Start)
        
        Hotbar.disable()
        Cam.connect("showcase_finished", self, "showcase_complete")
        Cam.showcase(showcase_path, showcase_time, showcase_delay)
    else:
        Cam.set_pos(Start.position - G.HScreen)
        Player.respawn()
    
    UI.connect("hatch", Player, "kill")
    UI.connect("restart", self, "reload")
    UI.connect("follow", self, "follow_toggle")
    RetryBtn.connect("pressed", self, "reload")
    NextLvlBtn.connect("pressed", self, "next_level")
    
func showcase_complete():
    Player.respawn()
    Hotbar.enable()
    $CanvasLayer/ClickToPlay.hide()

func _process(_delta):
    if Cam.is_showcasing() or level_complete:
        skip_timer -= _delta
        if skip_timer <= 0:
            $CanvasLayer/ClickToPlay.show()
        return
        
    if follow_ducky and Player.alive:
        Cam.set_pos(Player.position - Vector2(G.Screen.x/2, G.Screen.y/2))
    
    if _tool:
        var mpos = get_viewport().get_mouse_position() + Cam.position
        var nearest = Map.find_nearest_valid(mpos, _tool.is_flying)
        if nearest and location_empty(nearest):
            _tool.position = nearest
        
    if Player.alive and Player.position.distance_to(Goal.position) < 5:
        if demo:
            respawn()
        else:
            complete_level()

func complete_level():
    Player.reached_mama()
    Goal.reached_mama()
    Instructions.hide()
    LevelComplete.show()
    
func next_level():
    G.next_level(self.name)
    Stats.levels_complete += 1

func location_empty(pos: Vector2) -> bool:
    for obstacle in $TileMap/Obstacles.get_children():
        if G.lock_to_tile(obstacle.position).distance_squared_to(pos) < 1:
            return false
    for tol in $TileMap/Tools.get_children():
        if G.lock_to_tile(tol.position).distance_squared_to(pos) < 1:
            return false
    return true

func tool_clicked(tool_scene: PackedScene):
    _tool = tool_scene.instance()
    _tool.disable()
    $TileMap/Tools.add_child(_tool)
    _tool.position = get_viewport().get_mouse_position()
    _click_timer = OS.get_ticks_msec()
    
func place_tool():
    _tool.enable()
    _tool = null

func follow_toggle(follow_state: bool):
    self.follow_ducky = follow_state
    
func respawn():
    Player.position = Start.position
    Player.respawn()
    
func reload():
    Stats.deaths += 1
    var _err = get_tree().reload_current_scene()
    
func _calc_tilemap_size(tilemap):
    var start = tilemap.get_used_rect().position*G.GridSize + Map.position
    var end = start + tilemap.get_used_rect().size*G.GridSize
    if UI.visible:
        end.y += Hotbar.size().y
    return [start, end]
    
func _calc_max_tilemap_size():
    var main_map = _calc_tilemap_size(Map)
    var bg_map = _calc_tilemap_size(BGMap)
    var start = Vector2(min(main_map[0].x, bg_map[0].x), min(main_map[0].y, bg_map[0].y))
    var end = Vector2(max(main_map[1].x, bg_map[1].x), max(main_map[1].y, bg_map[1].y))
    var _size = end - start
    return [_size, start]
    
func _input(event):
    if event is InputEventMouseButton:
        if Cam.is_showcasing() and skip_timer < 0:
            Cam.stop_showcase()
            $CanvasLayer/ClickToPlay.hide()
            showcase_complete()
            return
        
        var iemb: InputEventMouseButton = event as InputEventMouseButton
        if _tool and iemb.button_index == BUTTON_LEFT and (iemb.pressed or OS.get_ticks_msec() - _click_timer > 150):
            place_tool()
        elif _tool and iemb.button_index == BUTTON_RIGHT and iemb.pressed:
            $TileMap/Tools.remove_child(_tool)
            Hotbar.replace_tool(_tool)
            _tool.queue_free()
            
            _tool = null
    if event is InputEventKey:
        var iek: InputEventKey = event as InputEventKey
        if iek.scancode == KEY_Z and event.pressed:
            reload()
            
func _unhandled_input(event):
    if event.is_action_pressed("restart"):
        respawn()








