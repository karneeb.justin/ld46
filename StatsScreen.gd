extends "res://levels/Level.gd"

onready var restart_btn = $CanvasLayer/Restart
onready var deaths_label = $CanvasLayer/Labels/DeathN
onready var completed_label = $CanvasLayer/Labels/LevelN
onready var breads_label = $CanvasLayer/Labels/BreadN
var prev_volume: float = 0

func _ready():
    deaths_label.text = str(Stats.deaths)
    completed_label.text = str(Stats.levels_complete)
    breads_label.text = str(Stats.coins_collected)
    
    prev_volume = Settings.music_vol
    MusicPlayer.volume_db = min(prev_volume, -35)
    VictoryPlayer.play()
    
    restart_btn.connect("pressed", self, "restart_game")
    
func restart_game():
    MusicPlayer.volume_db = prev_volume
    VictoryPlayer.stop()
    G.restart_game()
