extends CollisionShape2D

func _ready():
    $Area2D/CollisionShape2D.shape = shape
    var _err = $Area2D.connect("body_entered", self, "_body_entered")
    
func _body_entered(body: Node):
    body.kill()
