extends "res://obstacles/Obstacle.gd"

func _ready():
    var _err = $StandZone.connect("body_entered", self, "_stand")
    _err = $SwatZone.connect("body_entered", self, "_swat")

func _stand(_body):
    if not $Sprite/AnimationPlayer.is_playing():
        $Sprite/AnimationPlayer.play("tiger stand")
    
func _swat(_body):
    if not $Sprite/AnimationPlayer.is_playing():
        $Sprite/AnimationPlayer.play("tiger swat")
