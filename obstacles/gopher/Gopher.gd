extends "res://obstacles/Obstacle.gd"

onready var player = $Offset/LeftHand/AnimationPlayer
onready var kzr = $KillZone/CollisionShape2D_R
onready var kzl = $KillZone/CollisionShape2D
onready var gzr = $GrabZone/CollisionShape2D
onready var gzl = $GrabZone/CollisionShape2D_L

var grabbing: bool = false

func _ready():
    var _err = player.connect("animation_finished", self, "_anim_finished")
    _err = $GrabZone.connect("body_entered", self, "_grab")

func _process(_delta: float):
    if ducky:
        var dir = ducky.position.x - position.x
        var ducky_dir = ducky.dir
        
        if dir < 0 and ducky_dir > 0:
            kzl.disabled = false
            kzr.disabled = true
            gzl.disabled = true
            gzr.disabled = false
        elif dir > 0 and ducky_dir < 1:
            kzl.disabled = true
            kzr.disabled = false
            gzl.disabled = false
            gzr.disabled = true

func _hit(_body: Node):
    player.play("gopher grab")
    grabbing = true
    
func _grab(_body: Node):
    var exit = find_node("Exit", false, true)
    if player.is_playing() and player.current_animation == "gopher grab":
        _body.grab(exit)
        $KillPlayer.play_random()

func _anim_finished(anim: String):
    if anim == "gopher grab":
        grabbing = false
        player.play("gopher idle")
