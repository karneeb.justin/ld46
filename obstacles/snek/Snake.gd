extends "res://obstacles/Obstacle.gd"

export(float) var move_speed: float = 64
export(float) var move_duration: float = 2
export(float) var move_delay: float = 1
var move_dir: Vector2 = Vector2(1, 0)
var move_time: float = move_duration


func _process(delta: float):
    move_time -= delta
    if move_time >= 0:
        self.position += move_dir * move_speed * delta
    elif move_time < -move_delay:
        move_dir.x = -move_dir.x
        move_time = move_duration
        scale.x = -scale.x
        if scale.x < 0:
            position.x -= G.GridSize
        else:
            position.x += G.GridSize
