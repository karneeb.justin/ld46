extends "res://TileNode.gd"
export(bool) var always_face: bool = true
var ducky = null

func _ready():
    var _err = $KillZone.connect("body_entered", self, "_hit")
    _err = $DetectionArea.connect("body_entered", self, "_start_change_facing")
    _err = $DetectionArea.connect("body_exited", self, "_end_change_facing")

func _process(_delta: float):
    if ducky and always_face:
        var dir = ducky.position.x - position.x
        var ducky_dir = ducky.dir
        
        if dir < 0 and ducky_dir > 0:
            scale.x = 1
        elif dir > 0 and ducky_dir < 1:
            scale.x = -1

func _start_change_facing(body: Node):
    ducky = body

func _end_change_facing(_body: Node):
    ducky = null

func _hit(body):
    body.kill()
    $KillPlayer.play_random()
