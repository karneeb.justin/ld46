extends "res://obstacles/Obstacle.gd"

func _ready():
    var _err = $TriggerZone.connect("body_entered", self, "_crush")
    
func _crush(_body):
    $AnimationPlayer.play("crush")
    $SlinkPlayer.play_random()
