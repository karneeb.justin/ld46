extends "res://tools/Tool.gd"

func _body_entered(body: Node):
    ._body_entered(body)
    hide()
    
    Stats.coins_collected += 1
    $ActiveArea.set_deferred("monitoring", false)
    $TriggerPlayers/got.connect("finished", self, "finished", [], CONNECT_ONESHOT)
    
func finished():
    get_parent().call_deferred("remove_child", self)
    queue_free()
