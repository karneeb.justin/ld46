extends "res://tools/Tool.gd"

export(float) var fall_modifier: float = 0.5
export(int) var num_provided: int = 1

func _ready():
    var _err = $TriggerPlayers/happy.connect("finished", self, "cleanup")

func _body_entered(body: Node):
    if body.alive and not body.corpse:
        ._body_entered(body)
        body.parachute(fall_modifier, num_provided)
        self.disable()
        self.hide()
    
func cleanup():
    get_parent().call_deferred("remove_child", self)
    self.queue_free()
