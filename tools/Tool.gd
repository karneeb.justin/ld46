extends "res://TileNode.gd"

export(String) var tool_name: String
export(Vector2) var ui_offset: Vector2 = Vector2(0, 0)
export(float) var ui_scale: float = 1
export(bool) var is_flying: bool = false
export(bool) var always_face: bool = true
export(bool) var start_disabled: bool = false
var ducky = null
var is_ui = false

func _ready():
    var _err = $ActiveArea.connect("body_entered", self, "_body_entered")
    _err = $DetectionArea.connect("body_entered", self, "_start_change_facing")
    _err = $DetectionArea.connect("body_exited", self, "_end_change_facing")
    
    if start_disabled:
        disable()

func _body_entered(_body: Node):
    $TriggerPlayers.play_random()

func _process(_delta: float):
    if ducky and always_face:
        var dir = ducky.position.x - position.x
        var ducky_dir = ducky.dir
        
        if dir < 0 and ducky_dir > 0:
            scale.x = 1
        elif dir > 0 and ducky_dir < 1:
            scale.x = -1

func _start_change_facing(body: Node):
    ducky = body

func _end_change_facing(_body: Node):
    ducky = null
    
func disable():
    $ActiveArea.set_deferred("monitoring", false)
    $DetectionArea.monitoring = false
    
func enable():
    $ActiveArea.monitoring = true
    $DetectionArea.monitoring = true
    
func make_ui_tool() -> Vector2:
    disable()
    scale = Vector2(ui_scale, ui_scale)
    
    return ui_offset
