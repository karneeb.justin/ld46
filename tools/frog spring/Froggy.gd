extends "res://tools/Tool.gd"

export(float) var force = 300
export(float) var duration = 0.25

func _body_entered(body: Node):
    ._body_entered(body)
    body.jump(force, duration)
    $AnimationPlayer.play("toad jump")
