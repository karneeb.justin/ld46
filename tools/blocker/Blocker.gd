extends "res://tools/Tool.gd"

func disable():
    .disable()
    $CollisionArea/CollisionShape2D.disabled = true
    
func enable():
    .enable()
    $CollisionArea/CollisionShape2D.disabled = false
