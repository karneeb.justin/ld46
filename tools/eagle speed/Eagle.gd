extends "res://tools/Tool.gd"

export(float) var dash_speed = 600
export(float) var dash_duration = 0.5

func _ready():
    var _err = $TriggerArea.connect("body_entered", self, "_trigger_wing")

func _trigger_wing(_body: Node):
    $Sprite/AnimationPlayer.play("eagle prep")

func _body_entered(body: Node):
    ._body_entered(body)
    $Sprite/AnimationPlayer.play("eagle whoosh")
    body.dash(dash_speed, dash_duration)
    
func disable():
    .disable()
    $TriggerArea.monitoring = false
    
func enable():
    .enable()
    $TriggerArea.monitoring = true
