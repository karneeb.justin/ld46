extends "res://tools/Tool.gd"

export(float) var dash_speed = 600
export(float) var dash_duration = 0.5

func _body_entered(body: Node):
    body.dash(dash_speed, dash_duration)
