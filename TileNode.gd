extends Node2D

export(bool) var ignore_tiles: bool = false

func _ready():
    if not ignore_tiles:
        var pos = self.position
        pos.x = floor(pos.x/G.GridSize) * G.GridSize + G.GridSize/2
        pos.y = floor(pos.y/G.GridSize) * G.GridSize + G.GridSize/2
        self.position = pos
