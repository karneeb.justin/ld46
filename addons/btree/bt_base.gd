extends Node

export(bool) var debuggable := false
export(bool) var disabled := false

var last_status := Status.Success
var _debug := true

func tick(delta: float, blackboard: Blackboard, debug: bool = true) -> String:
    _debug = debug
    if last_status != Status.Running:
        start(delta, blackboard)
    
    last_status = update(delta, blackboard)
    blackboard.get("debugger").update(self, last_status)

    return last_status

func start(delta: float, blackboard: Blackboard) -> void:
    pass

func update(delta: float, blackboard: Blackboard) -> String:
    return Status.Success

func get_node_type() -> String:
    return 'base'

func reset() -> void:
    last_status = Status.Failed
    for child in get_children():
        if child.has_method("reset"):
            child.reset()
