extends Node

export(bool) var enabled := false
export(String) var display_name := "BehaviorTree"

var key := "debugger"
var data := self

var last_node = null
var last_status: String = ""

func update(node, status) -> void:
    if !enabled || !node._debug || !node.debuggable:
        return

    if node.get_node_type() == Status.Decorator || node.get_node_type() == Status.Composite:
        if status == Status.Running:
            return

    if last_node != node:
        last_node = node
        last_status = ""
    
    if last_status != status:
        last_status = status
        var pretty_status = Status.pretty(status)
        print(get_depth(node) + " " + display_name + " -> " + node.name +" [" + pretty_status + "]")

func get_depth(node) -> String:
    var depth = ""
    while node.name != "Root":
        node = node.get_parent()
        depth += '-'

    return depth
