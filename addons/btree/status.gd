class_name Status

const Running := 'r'
const Failed := 'f'
const Success := 's'

const Decorator := "decorator"
const Composite := "composite"
const Monitor := "monitor"

static func pretty(status: String) -> String:
    if status == Running:
        return "Running"
    elif status == Failed:
        return "Failed"
    elif status == Success:
        return "Success"
    return "Unknown"
