extends Node
class_name BehaviorTree

onready var blackboard := $Blackboard
onready var root := $Root

var status = Status.Success

func run(delta) -> void:
    for monitor in blackboard._monitors:
        var mon_result = monitor.check(delta, blackboard)
        if mon_result == Status.Success:
            blackboard.clear_monitors()
            root.reset()
            break

    if status != Status.Running:
        blackboard.clear_monitors()
        root.reset()
    status = root.tick(delta, blackboard)

func reset() -> void:
    for child in get_children():
        if child.has_method("reset"):
            child.reset()
