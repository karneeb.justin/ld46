extends "res://addons/btree/composite/composite.gd"

export(bool) var can_fail := true

func update(delta: float, blackboard: Blackboard) -> String:
    var status = Status.Success

    for i in range(0, get_child_count()):
        var child = get_child(i)
        if child.disabled:
            continue
        
        var child_status = child.tick(delta, blackboard, _debug)
        if child_status == Status.Failed:
            if can_fail:
                if reset_on_fail:
                    reset()
                return child_status
            else:
                status = Status.Running
        elif child_status == Status.Running:
            status = Status.Running

    return status
