extends "res://addons/btree/composite/composite.gd"

var active_child: int = 0

func start(_delta: float, _blackboard: Blackboard) -> void:
    active_child = 0

func update(delta: float, blackboard: Blackboard) -> String:
    var status = Status.Failed

    for i in range(active_child, get_child_count()):
        var child = get_child(i)
        if child.disabled:
            continue
        
        status = child.tick(delta, blackboard, _debug)
        if status != Status.Failed:
            if status == Status.Running and continue_from_running:
                active_child = i
            return status

    if reset_on_fail:
        reset()
    return Status.Failed

func reset() -> void:
    if propogate_reset:
        .reset()
        active_child = 0
