extends "res://addons/btree/bt_base.gd"

export(bool) var continue_from_running := true
export(bool) var propogate_reset := false
export(bool) var reset_on_fail := false

func get_node_type() -> String:
    return Status.Composite
