extends Node
class_name Blackboard

var _data := {}
var _monitors := []
var _self = null

func _enter_tree():
    pass

func _ready():
    for child in get_children():
        if typeof(child.data) == TYPE_NODE_PATH:
            _data[child.key] = get_node(str(child.data).right(3))
        else:
            _data[child.key] = child.data

func set_data(bb_data) -> void:
    _data[bb_data.key] = bb_data.data
    self.add_child(bb_data)

func reset() -> void:
    _data = {}
    _monitors = []
    _ready()

func set(key, value) -> void:
    _data[key] = value

func get(key):
    if _data.has(key):
        return _data[key]
    if key == 'self':
        return _self
    return null

func add_monitor(monitor) -> void:
    _monitors.push_back(monitor)
    
func clear_monitors() -> void:
    _monitors = []
