extends "res://addons/btree/decorator/decorator.gd"

func update(delta: float, blackboard: Blackboard) -> String:
    var status = Status.Success
    var child = get_child(0)
    while status == Status.Success:
        status = child.tick(delta, blackboard)

    return status
