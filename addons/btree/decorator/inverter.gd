extends "res://addons/btree/decorator/decorator.gd"

func update(delta: float, blackboard: Blackboard) -> String:
    var child = get_child(0)
    var status = child.tick(delta, blackboard)

    if status == Status.Running:
        return status
    elif status == Status.Success:
        return Status.Failed
    else:
        return Status.Success
