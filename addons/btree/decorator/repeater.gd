extends "res://addons/btree/decorator/decorator.gd"

export(int) var num_repeats: int = 1
onready var repeats_remaining: int = num_repeats

func start(delta: float, blackboard: Blackboard) -> void:
    repeats_remaining = num_repeats

func update(delta: float, blackboard: Blackboard) -> String:
    var status = Status.Success
    var child = get_child(0)
    while repeats_remaining != 0:
        status = child.update(delta, blackboard)
        if status == Status.Running:
            return Status.Running
        repeats_remaining -= 1

    return status
