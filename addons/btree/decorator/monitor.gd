extends "res://addons/btree/bt_base.gd"

func update(delta: float, blackboard: Blackboard) -> String:
    var child = get_child(0)
    var status = child.tick(delta, blackboard, _debug)

    if status == Status.Failed:
        blackboard.add_monitor(self)
    return status

func check(delta: float, blackboard: Blackboard) -> String:
    var child = get_child(0)
    var status = child.tick(delta, blackboard, false)
    return status

func get_node_type():
    return Status.Monitor
