extends "res://addons/btree/decorator/decorator.gd"

enum RETURN_VAL {SUCCESS, FAILURE, RUNNING}

export(RETURN_VAL) var return_val: int = 0
export(bool) var propogate_running := false

func update(delta: float, blackboard: Blackboard) -> String:
    var status = get_child(0).tick(delta, blackboard)
    
    if propogate_running and status == Status.Running:
        return Status.Running
    
    if return_val == RETURN_VAL.SUCCESS:
         return Status.Success
    elif return_val == RETURN_VAL.RUNNING:
        return Status.Running
    return Status.Failed
