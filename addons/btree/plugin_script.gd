tool
extends EditorPlugin

func _enter_tree():
    add_custom_type(
        "BehaviorTree",
        "Node",
        preload("res://addons/btree/behavior_tree.gd"),
        preload("res://addons/btree/root_icon.png")
    )

    add_custom_type(
        "Blackboard",
        "Node",
        preload("res://addons/btree/blackboard/blackboard.gd"),
        preload("res://addons/btree/blackboard/blackboard_icon.png")
    )

    add_custom_type(
        "BlackboardData",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BlackboardDataInt",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data_int.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BlackboardDataFloat",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data_float.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BlackboardDataString",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data_string.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BlackboardDataObject",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data_object.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BlackboardDataNode",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data_node.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BlackboardDataNodePath",
        "Node",
        preload("res://addons/btree/blackboard/blackboard_data_nodepath.gd"),
        preload("res://addons/btree/blackboard/blackboard_data_icon.png")
    )

    add_custom_type(
        "BehaviorSequence",
        "Node",
        preload("res://addons/btree/composite/sequence.gd"),
        preload("res://addons/btree/composite/sequence_icon.png")
    )

    add_custom_type(
        "BehaviorSelector",
        "Node",
        preload("res://addons/btree/composite/selector.gd"),
        preload("res://addons/btree/composite/selector_icon.png")
    )

    add_custom_type(
        "BehaviorParallel",
        "Node",
        preload("res://addons/btree/composite/parallel.gd"),
        preload("res://addons/btree/composite/parallel_icon.png")
    )

    add_custom_type(
        "BehaviorInverter",
        "Node",
        preload("res://addons/btree/decorator/inverter.gd"),
        preload("res://addons/btree/decorator/inverter_icon.png")
    )

    add_custom_type(
        "BehaviorRepeater",
        "Node",
        preload("res://addons/btree/decorator/repeater.gd"),
        preload("res://addons/btree/decorator/repeater_icon.png")
    )

    add_custom_type(
        "BehaviorRepeatUntilFail",
        "Node",
        preload("res://addons/btree/decorator/repeat_until_fail.gd"),
        preload("res://addons/btree/decorator/repeat_until_fail_icon.png")
    )

    add_custom_type(
        "BehaviorAlways",
        "Node",
        preload("res://addons/btree/decorator/always.gd"),
        preload("res://addons/btree/decorator/always_icon.png")
    )

    add_custom_type(
        "BehaviorMonitor",
        "Node",
        preload("res://addons/btree/decorator/monitor.gd"),
        preload("res://addons/btree/decorator/monitor_icon.png")
    )

    add_custom_type(
        "BehaviorAction",
        "Node",
        preload("res://addons/btree/action.gd"),
        preload("res://addons/btree/action_icon.png")
    )

    add_custom_type(
        "BehaviorCondition",
        "Node",
        preload("res://addons/btree/condition.gd"),
        preload("res://addons/btree/condition_icon.png")
    )

    add_custom_type(
        "BehaviorDebugger",
        "Node",
        preload("res://addons/btree/debugger.gd"),
        preload("res://addons/btree/debugger_icon.png")
    )

func _exit_tree():
    remove_custom_type("BehaviorTree")
    remove_custom_type("Blackboard")
    remove_custom_type("BlackboardData")
    remove_custom_type("BlackboardDataInt")
    remove_custom_type("BlackboardDataFloat")
    remove_custom_type("BlackboardDataString")
    remove_custom_type("BlackboardDataObject")
    remove_custom_type("BlackboardDataNode")
    remove_custom_type("BlackboardDataNodePath")
    remove_custom_type("BehaviorSequence")
    remove_custom_type("BehaviorSelector")
    remove_custom_type("BehaviorParallel")
    remove_custom_type("BehaviorInverter")
    remove_custom_type("BehaviorRepeater")
    remove_custom_type("BehaviorRepeatUntilFail")
    remove_custom_type("BehaviorAlways")
    remove_custom_type("BehaviorMonitor")
    remove_custom_type("BehaviorAction")
    remove_custom_type("BehaviorCondition")
    remove_custom_type("BehaviorDebugger")

