class_name BaseCurve

var maximum: float = 1

func reset(maximum: float) -> void:
    self.maximum = maximum
    
func eval(dist: float) -> float:
    return _eval(dist)
    
func _eval(dist: float) -> float:
    return 0.0
