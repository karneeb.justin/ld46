class_name LinearCurve extends BaseCurve

func _eval(dist: float) -> float:
    return (self.maximum - dist) / self.maximum
