shader_type canvas_item;

uniform sampler2D influence;
uniform sampler2D costs;
uniform float elapsed;
uniform float decay_rate;
uniform float transmission;
uniform float blur;

void fragment() {
    vec2 psize = 1.0 / vec2(textureSize(influence, 0));

    vec2 dv = vec2(UV.x, UV.y + psize.y);
    vec2 uv = vec2(UV.x, UV.y - psize.y);
    vec2 rv = vec2(UV.x + psize.x, UV.y);
    vec2 lv = vec2(UV.x - psize.x, UV.y);

    float cost = texture(costs, UV).x;
    float c = textureLod(influence, UV, blur).x;
    if( UV.x <= 0.01 || UV.x >= 0.99 || UV.y <= 0.01 || UV.y >= 0.99 || cost > 0. )
        c = 0.;

    float ux = 0.;
    float dx = 0.;
    float rx = 0.;
    float lx = 0.;

    if( uv.y > 0.0 && uv.y < 1.0 )
        ux = texture(influence, uv).x - cost - transmission;
    if( dv.y > 0.0 && dv.y < 1.0 )
        dx = texture(influence, dv).x - cost - transmission;
    if( rv.x > 0.0 && rv.x < 1.0 )
        rx = texture(influence, rv).x - cost - transmission;
    if( lv.x > 0.0 && lv.x < 1.0 )
        lx = texture(influence, lv).x - cost - transmission;

    c = max(c, max(ux, max(dx, max(rx, lx))));
    c = max(0, c - elapsed * decay_rate);

    COLOR.x = c;
    COLOR.y = texture(costs, UV).x;
}