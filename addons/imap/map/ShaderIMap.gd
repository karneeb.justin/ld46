extends Control

onready var vp = $Viewport
onready var shader = $Viewport/ColorRect.material
onready var render = $Render

var imap: Image
var costs: Image
var imap_arr
var elapsed: float
var elapsed2: float

export(int) var width: int
export(int) var height: int
export(bool) var visualize: bool = false
export(float) var render_scale: float = 10
export(float) var update_rate: float = 0.1
export(float) var decay_rate: float = 1.0
export(float) var transmission_cost: float = 0.1
export(float) var blur: float = 0.0

export(String) var emitter_group: String
export(float) var emitter_frequency: float

func _ready():
    vp.size = Vector2(width, height)
    render.rect_scale = Vector2(render_scale, render_scale)
    
    imap = Image.new()
    imap.create(self.width, self.height, false, Image.FORMAT_RGBAH)
    imap_arr = imap.get_data()

    costs = Image.new()
    costs.create(self.width, self.height, false, Image.FORMAT_RGBAH)
    
    shader.set_shader_param("elapsed", update_rate)
    shader.set_shader_param("transmission", transmission_cost)        
    shader.set_shader_param("decay_rate", decay_rate)
    shader.set_shader_param("blur", blur)
    shader.set_shader_param("influence", _create_tex(imap))
    shader.set_shader_param("costs", _create_tex(costs))
    
    if not visualize:
        render.hide()
    
func clear_influcence():
    imap.lock()
    for x in range(self.width):
        for y in range(self.height):
            imap.set_pixel(x, y, Color(0, 0, 0, 1))
    imap.unlock()
    
    imap_arr = imap.get_data()
    shader.set_shader_param("influence", _create_tex(imap))
    read_influence()

func read_influence():
    vp.render_target_update_mode = Viewport.UPDATE_ONCE
    yield(VisualServer, "frame_post_draw")
    var new_imap = vp.get_texture().get_data()
    new_imap.lock()
    imap.create_from_data(width, height, false, Image.FORMAT_RGBAH, new_imap.get_data())
    new_imap.unlock()
    
    imap_arr = imap.get_data()
    shader.set_shader_param("influence", _create_tex(imap))

func add_rand():
    var x = rand_range(0, width)
    var y = rand_range(0, height)
    add(x, y, 1.0)    
    
func add(x: int, y: int, val: float) -> void:
    imap.lock()
    imap.set_pixel(x, y, Color(val+.3, 0.0, 0.0, 1.0))
    imap.unlock()
    
    shader.set_shader_param("influence", _create_tex(imap))
    read_influence()
    
func _batch_add():
    imap.lock()
    
func _batch_end():
    imap.unlock()
    shader.set_shader_param("influence", _create_tex(imap))
    read_influence()
    
func get_inf(x: int, y: int) -> float:
    x /= render_scale
    y /= render_scale
    x = clamp(x, 0, width-1)
    y = clamp(y, 0, height-1)
    
    var index = (y * width + x) * 8    
    var h: int = imap_arr[index] | imap_arr[index+1] << 8
    var f: float = _float32(h)
    return f
    
func getv_inf(loc: Vector2) -> float:
    return get_inf(loc.x, loc.y)
    
func extract_radius(loc: Vector2, radius: int) -> IMap:
    return add_radius_to_map(loc, radius, IMap.new(radius*2, radius*2, render_scale))

func add_radius_to_map(loc: Vector2, radius: int, imap: IMap) -> IMap:
    for i in range(-radius, radius):
        for j in range(-radius, radius):
            var inf = get_inf(loc.x+i*render_scale, loc.y+j*render_scale)
            imap.addv(i+radius, j+radius, inf)
    return imap
    
func add_cost(x: int, y: int, val: float) -> void:
    costs.lock()
    costs.set_pixel(x, y, Color(val, 0.0, 0.0, 1.0))
    costs.unlock()
    
    shader.set_shader_param("costs", _create_tex(costs))
    
func _create_tex(img: Image) -> Texture:
    var tex: ImageTexture = ImageTexture.new()
    tex.create_from_image(img)
    return tex
    
func _process(delta):
    elapsed += delta
    if elapsed > update_rate:
        shader.set_shader_param("elapsed", elapsed)
        read_influence()
        elapsed = 0
        
    elapsed2 += delta
    if elapsed2 > emitter_frequency:
        _batch_add()
        for node in get_tree().get_nodes_in_group(emitter_group):
            var pos: Vector2 = node.global_position / 10
            pos.x = clamp(pos.x, 0, 105)
            pos.y = clamp(pos.y, 0, 59)
            imap.set_pixel(pos.x, pos.y, Color(5, 0, 0, 1))
        _batch_end()
        elapsed2 = 0
        
    if Input.is_mouse_button_pressed(BUTTON_RIGHT):
        var pos = get_viewport().get_mouse_position()
        add_cost(pos.x/render_scale, pos.y/render_scale, 100)
        
func _input(event):
    if event is InputEventMouseButton:
        var mevent = event as InputEventMouseButton
        if mevent.is_pressed():
            if mevent.button_index == BUTTON_LEFT:
                var pos = mevent.position
                add(pos.x/render_scale, pos.y/render_scale, 1)
            elif mevent.button_index != BUTTON_RIGHT:
                clear_influcence()

var stream: StreamPeerBuffer = StreamPeerBuffer.new()
func _float32(i):
        var t1
        var t2
        var t3

        t1 = i & 0x7fff                       # Non-sign bits
        t2 = i & 0x8000                       # Sign bit
        t3 = i & 0x7c00                       # Exponent
        
        t1 <<= 13                              # Align mantissa on MSB
        t2 <<= 16                              # Shift sign bit into position

        t1 += 0x38000000                       # Adjust bias

        if t3 == 0:
            t1 = 0                              # Denormals-as-zero

        t1 |= t2;                               # Re-insert sign bit

        stream.seek(0)
        stream.put_32(t1)
        stream.seek(0)
        var f = stream.get_float()
        return f
