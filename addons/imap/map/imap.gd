class_name IMap

signal changed

var anchor: Vector2
var cell_size: float
var height: int
var width: int
var grid: Array

func _init(width: int, height: int, size: float):
    self.width = width
    self.height = height
    self.cell_size = size

    grid = []
    for i in range(self.width):
        grid.append([])
        for j in range(self.height):
            grid[i].append(0.0)

func propogate(cx: int, cy: int, radius: int, prop: BaseCurve, magnitude: float) -> void:
    var half_radius: int = radius/2
    var sx: int = max(0, cx - half_radius)
    var sy: int = max(0, cy - half_radius)
    var ex: int = min(self.width, cx + half_radius)
    var ey: int = min(self.height, cy + half_radius)

    prop.reset(half_radius)
    for x in range(sx, ex+1):
        for y in range(sy, ey+1):
            var dist = sqrt(pow(x-cx, 2) + pow(y-cy, 2))
            if dist <= half_radius:
                self.addv(x, y, prop.eval(dist) * magnitude)
    emit_signal("changed")
            
func addm(source: IMap, destx_center: int, desty_center: int, magnitude: float = 1.0, offsetx: int = 0, offsety: int = 0) -> void:
    var sx = destx_center + offsetx - (source.width/2)
    var sy = desty_center + offsety - (source.height/2)
    
    for x in range(source.width+1):
        for y in range(source.height+1):
            var tx: int = x + sx
            var ty: int = y + sy            
            addv(tx, ty, source.getv(x, y) * magnitude)
    emit_signal("changed")
            
func liftm(target: IMap, srcx_center: int, srcy_center: int, magnitude: float = 1.0, default: float = 0, offsetx: int = 0, offsety: int = 0) -> void:
    var sx = srcx_center + offsetx - (target.width/2)
    var sy = srcy_center + offsety - (target.height/2)
    
    for x in range(target.width):
        for y in range(target.height):
            var sourcex = sx + x
            var sourcey = sy + y
            target.addv(x, y, getv(sourcex, sourcey, default) * magnitude)
    target.emit_signal("changed")

func setv(x: int, y: int, val: float, emit: bool = false) -> void:
    if x >= 0 && x < self.width && y >= 0 && y < self.height:
        self.grid[x][y] = val
        if emit:
            emit_signal("changed")

func getv(x: int, y: int, default: float = 0) -> float:
    if x >= 0 && x < self.width && y >= 0 && y < self.height:
        return self.grid[x][y]
    return default

func addv(x: int, y: int, val: float, emit: bool = false) -> void:
    if x >= 0 && x < self.width && y >= 0 && y < self.height:
        self.grid[x][y] += val
        if emit:
            emit_signal("changed")
            
func find_peak(high:bool=true, x:int=0, y:int=0, width:int=self.width, height:int=self.height) -> Vector2:
    var peak: float = -INF if high else INF
    var peak_loc = Vector2(-1, -1)
    
    var xs = range(x, width)
    var ys = range(y, height)
    xs.shuffle()
    ys.shuffle()
    
    for i in xs:
        for j in ys:
            var val = self.grid[i][j]
            if (high and val > peak) or (not high and val < peak):
                peak = val
                peak_loc = Vector2(i, j)
    return peak_loc
                

func pretty_print() -> void:
    for row in grid:
        print(row)

func _to_string() -> String:
    return "Height: %s, Width: %s" % [self.width, self.height]
