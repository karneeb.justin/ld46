extends Node2D

var _imap: IMap

func set_imap(imap: IMap) -> void:
    self._imap = imap
    #self.scale = Vector2(imap.cell_size, imap.cell_size)
    
    for child in $Grid.get_children():
        child.queue_free()
        $Grid.remove_child(child)
        
    
        
#    $Grid.columns = _imap.width
#    for x in range(_imap.width):
#        for y in range(_imap.height):
#            var tile = $Blank.duplicate()
#            tile.show()
#            $Grid.add_child(tile)
#
    imap.connect("changed", self, "update")
    
func redraw():
    if _imap:
        for y in range(_imap.height):
            for x in range(_imap.width):
                var index = x + y * _imap.width
                var img = $Grid.get_child(index)
                
                var inf = _imap.grid[x][y]
                img.modulate = Color(inf, inf, inf, 1)

func _draw():
    if _imap:
        for y in range(_imap.height):
            for x in range(_imap.width):
                var inf = _imap.grid[x][y]
                if inf == 0:
                    continue
                
                var xloc = x * _imap.cell_size
                var yloc = y * _imap.cell_size
                draw_rect(Rect2(xloc, yloc, _imap.cell_size, _imap.cell_size), Color(inf, 0, 0, inf/2))
