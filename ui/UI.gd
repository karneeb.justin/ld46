extends Control

onready var HatchBtn = $Controls/Buttons/Hatch
onready var RestartBtn = $Controls/Buttons/Restart
onready var FollowBtn = $Controls/Buttons/Follow
onready var MusicSlider = $Controls/Center/Sliders/Music/MVol
onready var SFXSlider = $Controls/Center/Sliders/SFX/SFXVol

signal hatch()
signal restart()
signal follow(button_state)

func _ready():
    HatchBtn.connect("pressed", self, "emit_signal", ["hatch"])
    RestartBtn.connect("pressed", self, "emit_signal", ["restart"])
    FollowBtn.connect("toggled", self, "follow_toggle")
    MusicSlider.connect("value_changed", Settings, "update_music_vol")
    SFXSlider.connect("value_changed", Settings, "update_sfx_vol")
    
    MusicSlider.value = Settings.music_vol
    SFXSlider.value = Settings.sfx_vol
    if false:
        emit_signal("hatch")
        emit_signal("restart")
    
func follow_toggle(btn_state: bool):
    emit_signal("follow", btn_state)
