extends Control

onready var ToolContainer = $Container/HBoxContainer
signal tool_clicked(_tool)

func _ready():
    for hotbar_item in $Container/HBoxContainer.get_children():
        var _err = hotbar_item.connect("tool_clicked", self, "_clicked")
        
func _clicked(_tool):
    emit_signal("tool_clicked", _tool)

func size() -> Vector2:
    return $BG.rect_size
    
func replace_tool(_tool):
    for child in ToolContainer.get_children():
        if _tool.tool_name == child._tool.tool_name and child.quantity > -1:
            child.quantity += 1
            child.update_quant()
            
func enable():
    for child in ToolContainer.get_children():
        child.enable()
    
func disable():
    for child in ToolContainer.get_children():
        child.disable()
