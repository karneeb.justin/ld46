extends PanelContainer

signal tool_clicked(_tool)

export(PackedScene) var ToolScene : PackedScene
export(Texture) var ToolTexture: Texture
export(int) var quantity: int

onready var Portrait := $VBoxContainer/Portrait
onready var ToolName := $VBoxContainer/HBoxContainer/ToolName
onready var ToolAmount := $VBoxContainer/HBoxContainer/ToolAmount
onready var DisableTex := $VBoxContainer/Portrait/Disabled
onready var _tool = ToolScene.instance()

var disabled = false

func _ready():
    var tool_icon = ToolScene.instance()
    Portrait.texture = ToolTexture
    
    ToolName.text = tool_icon.tool_name
    tool_icon.queue_free()
    update_quant()
    
func _gui_input(event):
    if not disabled and event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
        if has_tool():
            emit_signal("tool_clicked", ToolScene)

func has_tool() -> bool:
    if quantity != 0:
        quantity -= 1
        update_quant()
        return true
    return false
    
func update_quant() -> void:
    ToolAmount.text = "" if quantity < 0 else ": " + str(quantity)
    if quantity == 0:
        DisableTex.show()
    else:
        DisableTex.hide()

func enable():
    disabled = false
    
func disable():
    disabled = true
    
