extends KinematicBody2D

onready var dash_timer = $DashTimer
onready var Backpack = $Graphics/UmbrellaBackpack
onready var Umbrella = $Umbrella
onready var UmbrellaAnim = $Umbrella/AnimationPlayer
onready var Anim = $Graphics/AnimationPlayer
onready var BodyAnim = $Graphics/Body/AnimationPlayer
onready var HatAnim = $Graphics/EggHat/AnimationPlayer

export(float) var speed: float = 200

var velocity: Vector2 = Vector2(speed, 0)
var dir = 1

var curr_speed: float = speed
var jump_duration: float = 0
var dash_duration: float = 0
var fall_modifier: float = 1
var num_parachutes: int = 0
var parachute_immunity: float = 0.2
var parachute_timer: float = 0
var death_friction: float = 1
var jumping: bool = false
var falling: bool = false
var grabbed: bool = false
var alive: bool = false
var corpse: bool = false
var at_mamma: bool = false
var grabbed_exit

func _ready():    
    dash_timer.connect("timeout", self, "_stop_dashing")    
        
func fall():
    if num_parachutes > 0:
        if not Umbrella.visible:
            UmbrellaAnim.play("inflate")
            UmbrellaAnim.queue("float")
            Umbrella.show()
            $ParachutePlayer.play_random()
        Backpack.hide()
    falling = true
    jump_duration = 0
        
func land():
    jumping = false
    falling = false
    if parachute_timer <= 0:
        num_parachutes = int(max(0, num_parachutes - 1))
    Umbrella.hide()
    if num_parachutes <= 0:
        Backpack.hide()
    else:
        Backpack.show()
    
func _physics_process(delta: float):    
    if grabbed or at_mamma:
        return
    
    if parachute_timer > 0:
        parachute_timer -= delta
    
    if jump_duration <= 0:
        var gravity = delta*9.8*100
        if (jumping or falling) and num_parachutes > 0 and velocity.y > 0:
            gravity *= fall_modifier
                
        var pre_vel = velocity.y
        velocity.y += gravity
        if pre_vel < 0 and velocity.y > 0:
            fall()
    elif jumping:
        jump_duration -= delta
        
    var pre_vely = velocity.y
    velocity.x = velocity.x * dir
    velocity = move_and_slide(velocity, Math.UP)
    
    if alive and velocity.x == 0:
        dir = -dir
        scale.x = -scale.x
    if velocity.y > 0 and not falling:
        fall()
    if velocity.y == 0:
        if pre_vely < 0:
            fall()
        elif falling:
            land()
    
    if alive:
        velocity.x = max(abs(velocity.x) - 500*delta, curr_speed)
    else:
        velocity.x = max(0, abs(velocity.x) - 100*delta*(rand_range(7, 13)/10))
    
func jump(force: float, duration: float):
    self.velocity.y = -force
    self.jump_duration = duration
    self.jumping = true
    self.falling = false

func kill():
    if alive and not corpse:
        alive = false
        var anim = randi()%2
        if anim == 0:
            Anim.play("Ded")
        else:
            Anim.play("death", -1, 2.5)
        BodyAnim.stop()
        HatAnim.stop()
        $AudioPlayers.pause()
        $DeathPlayers.play_random()
        Stats.deaths += 1
        
        yield(get_tree().create_timer(1), "timeout")
        respawn()
    
func dash(dash_speed: float, duration: float):
    self.curr_speed = dash_speed
    self.dash_timer.start(duration)
    
func parachute(fall_multiplier: float, num_uses: int):
    self.num_parachutes = int(max(num_uses, self.num_parachutes+num_uses))
    self.fall_modifier = fall_multiplier
    self.parachute_timer = self.parachute_immunity
    Backpack.show()
    
func grab(exit: Node):
    if not alive:
        return
    
    grabbed = true
    grabbed_exit = exit
    Anim.play("Grab")
    Anim.connect("animation_finished", self, "_grabbed_finished", [], CONNECT_ONESHOT)
    
func _grabbed_finished(_anim: String):
    if grabbed_exit:
        yield(get_tree().create_timer(0.65), "timeout")
        position = grabbed_exit.global_position - Vector2(32, 0)
        Anim.play_backwards("Grab")
        $GopherPlayer.play_random()
        yield(Anim, "animation_finished")
        grabbed = false
    else:
        kill()
    
func _stop_dashing():
    self.curr_speed = self.speed

func reached_mama():
    at_mamma = true
    Anim.play("Victory")
    
func respawn():
    if corpse:
        return
    
    grabbed = false
    alive = false
    if Anim.is_connected("animation_finished", self, "_grabbed_finished"):
        Anim.disconnect("animation_finished", self, "_grabbed_finished")
        
    var copy = self.duplicate()
    copy.corpse = true
    copy.alive = false
    copy.position = Vector2(self.position.x, self.position.y)
    copy.velocity = Vector2(self.velocity.x, self.velocity.y)
    copy.death_friction = 0.5 + randf()
    copy.dir = dir
    copy.scale = Vector2(scale.x, scale.y)
    copy.find_node("AudioPlayers").pause()
    copy.collision_layer = 0
    get_parent().add_child(copy)
        
    hide()
    self.velocity = Vector2(0, 0)
    self.position = G.SpawnPos
    
    Anim.play("Rest")
    yield(Anim, "animation_finished")
    $Graphics/Legs.hide()
    
    Anim.play("Hatch")
    yield(get_tree().create_timer(0.1), "timeout")
    show()
    yield(get_tree().create_timer(0.9), "timeout")
    $HatchPlayer.play_random()
    if scale.y < 0:
        scale.x = -1
    dir = 1
    
    alive = true
    self.velocity = Vector2(speed, 0)
    self.num_parachutes = 0
    BodyAnim.play("running bob")
    HatAnim.play("better bob")
    $Graphics/Legs.show()
    $AudioPlayers.resume()
