extends Node

onready var file := File.new()

func write(dict: Dictionary, path: String) -> void:
    if !path:
        return

    if file.open(path, File.WRITE) != OK:
        return

    var json_data := to_json(dict)
    file.store_string(json_data)
    file.close()

func read(path) -> Dictionary:
    if !path or !file.file_exists(path):
        return {}

    if file.open(path, File.READ) != OK:
        return {}

    var json_data := JSON.parse(file.get_as_text())
    file.close()

    return json_data.result
