extends Node

const UP := Vector2(0, -1)

static func int_rotation(vec: Vector2, dir: int) -> Vector2:
    var to_return = vec.rotated(deg2rad(90*dir))
    to_return.x = int(round(to_return.x))
    to_return.y = int(round(to_return.y))
    return to_return

static func man_dist(loc1: Vector2, loc2: Vector2) -> int:
    return int(abs(loc1.x - loc2.x) + abs(loc1.y - loc2.y))
    
static func man_radius(loc1: Vector2, loc2: Vector2) -> int:
    return int(min(abs(loc1.x - loc2.x), abs(loc1.y - loc2.y)))

static func get_angle_to(origin, target) -> float:
    return rad2deg(get_angle_to_r(origin, target))

static func get_angle_to_r(origin, target) -> float:
    var dir = UP.rotated(origin.global_rotation)
    var dir_to_target = (target.global_position - origin.global_position).normalized()
    var angle = atan2(dir_to_target.y, dir_to_target.x) - atan2(dir.y, dir.x)
    return normalize_angle_r(angle)

static func normalize_angle(angle) -> float:
    return fposmod(angle + 180, 360) - 180

static func normalize_angle_r(angle) -> float:
    return fposmod(angle + PI, 2*PI) - PI
