extends Node

var music_vol: float = -25
var sfx_vol: float = 0

func update_music_vol(vol: float):
    music_vol = vol
    if music_vol <= -50:
        MusicPlayer.volume_db = -INF
    else:
        MusicPlayer.volume_db = music_vol
    
func update_sfx_vol(vol: float):
    if vol <= -25:
        sfx_vol = -INF
    else:
        sfx_vol = vol
