extends Node

func throw(message, show_stack=false, show_tree=null):
    printerr(message)
    
    if show_stack:
        print_stack()

    if show_tree:
        show_tree.print_tree()