extends Node

var Screen: Vector2 = Vector2(1280, 720)
var HScreen: Vector2 = Screen/2

var GridSize: float = 64
var HGridSize: float = GridSize/2

var CamPos: Vector2 = Vector2(0, 0)
var SpawnPos: Vector2 = Vector2(0, 0)

var Tutorial = "res://levels/Tutorial.tscn"
var Levels = [Tutorial,
                "res://levels/LevelOne.tscn",
                "res://levels/LevelTwo.tscn",
                "res://levels/LevelGopher.tscn",
                "res://levels/LevelThree.tscn",
                "res://levels/LevelSuper.tscn",
                "res://levels/LevelFinal.tscn",
                "res://StatsLevel.tscn"]

func lock_to_tile(screen: Vector2) -> Vector2:
    var tile_pos = Vector2(GridSize * floor(screen.x/GridSize)+HGridSize, GridSize*floor(screen.y/GridSize+1) - HGridSize)
    return tile_pos

func next_level(curr_level: String):
    for i in range(Levels.size()):
        if Levels[i].find(curr_level) >= 0:
            var li = min(Levels.size()-1, i+1)
            var _err = get_tree().change_scene(Levels[li])
            return

func play_tutorial():
    Stats.reset()
    var _err = get_tree().change_scene(Tutorial)
    
func restart_game():
    Stats.reset()
    var _err = get_tree().change_scene(Levels[1])
    
