extends Node

var deaths: int = 0
var levels_complete: int = 0
var coins_collected: int = 0
var time_started: int = 0
var time_ended: int = 0

func reset():
    deaths = 0
    levels_complete = 0
    coins_collected = 0
    time_started = OS.get_ticks_msec()
    time_ended = -1
