extends Node

export(bool) var play_randomly = false
export(float) var random_freq: float = 2
export(float) var random_freq_randomness: float = 3
export(bool) var positional: bool = false
export(bool) var live_update: bool = false
export(float) var volume_mod: float = 0

onready var play_next = random_freq + random_freq_randomness*randf()
var paused: bool = false
var elapsed = 0
            
func _process(delta):
    if live_update:
        elapsed -= delta
        if elapsed < 0:
            get_child(0).volume_db = get_positional_volume() + volume_mod
            elapsed = 0.1
            
    if play_randomly:
        play_next -= delta
        if play_next < 0:
            play_random()
            play_next = random_freq+random_freq_randomness*randf()
        
func play_random():
    if not paused and not get_children().empty():
        var children = get_children()
        var child := children[randi()%children.size()] as AudioStreamPlayer
        
        if positional:
            child.volume_db = get_positional_volume() + volume_mod + Settings.sfx_vol
        else:
            child.volume_db = volume_mod + Settings.sfx_vol
        child.play()

func get_positional_volume():
    var pos: Vector2 = get_parent().position
    var cam_center = G.CamPos + G.HScreen
    var dist: float = cam_center.distance_to(pos)
    
    if dist < G.HScreen.x:
        return 0
    elif dist < G.Screen.x:
        var percent= 1 - (dist-G.HScreen.x)/G.HScreen.x
        return -25*percent
    return -100

func pause():
    self.paused = true
    
func resume():
    self.paused = false
